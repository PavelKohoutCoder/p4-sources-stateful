#include "headers.p4"
#include "parser.p4"
#include "cases/meters.p4"

// The IP firewall is working like follow:
// The incomming traffic is passed unchagend if we found the source IP address in the permit table.
// We can also tag it with some VLAN if required. In the case of hit, decrement the TTL value of IPv4 protocol
//
// If the IP address is not found, the vlan tagging is enabled and traffic is tagged to VLAN1 for further analysis.

control ingress {
   if(valid(ipv4)){
       // If the IPv4 is valid
       apply(table_ipv4_filter) {
           hit {
               apply(table_decrement_ttl);
           }
       }
    } else {
        // Vlan tagging
        apply(table_vlan_tag);
   }
}
