action add_vlan_tag(vid) {
    // Add the VLAN tag (overwrite if present)
    add_header(vlan_tag);

    // Setup the ethernet field value to vlan tag and copy the
    // ethertype value from ethernet to vlan tag. Copy the ether
    // type from the metadata field
    modify_field(vlan_tag.etherType,test_metadata.etherType);
    modify_field(ethernet.etherType,0x8100);

    // Index 0 is ignored, we are working with direct registers
    // the mapping tool fills the right index which is assigned to record.
    register_write(ethertypeReg,0,ethernet.etherType);
    register_read(ethernet.etherType,ethertypeReg,0);

    register_write(srcmacReg,0,ethernet.srcAddr);
    register_read(ethernet.srcAddr,srcmacReg,0);
    // Setup the VLAN tag
    modify_field(vlan_tag.pcp,0);
    modify_field(vlan_tag.cfi,0);
    modify_field(vlan_tag.vid,vid);

    // Sent the VLAN to the output port 3
    modify_field(intrinsic_metadata.egress_port,3);

}

action add_vlan_tag_no_reg(vid) {
    // Add the VLAN tag (overwrite if present)
    add_header(vlan_tag);

    // Setup the ethernet field value to vlan tag and copy the
    // ethertype value from ethernet to vlan tag. Copy the ether
    // type from the metadata field
    modify_field(vlan_tag.etherType,test_metadata.etherType);
    modify_field(ethernet.etherType,0x8100);

    // Setup the VLAN tag
    modify_field(vlan_tag.pcp,0);
    modify_field(vlan_tag.cfi,0);
    modify_field(vlan_tag.vid,vid);

    // Sent the VLAN to the output port 3
    modify_field(intrinsic_metadata.egress_port,3);

}

action _permit()  {
    no_op();
}

action _drop() {
drop();
}

action decrement_ttl() {
    add_to_field(ipv4.ttl,-1);
    register_write(decCnt,0,ipv4.ttl);
    register_read(ipv4.ttl,decCnt,0);
}

// Direct registers for the table_ip4_filter
register ethertypeReg {
    width   : 16;
    direct  : table_ipv4_filter;
}

// Direct registers for the table_ip4_filter
register srcmacReg {
    width   : 48;
    direct  : table_ipv4_filter;
}

table table_ipv4_filter {
    reads {
        ipv4.srcAddr : ternary;
    }

    actions {
        _permit;
        add_vlan_tag;
        _drop;
    }

    max_size: 511;
}

table table_vlan_tag {
    // No reads statement, always run tagging
    actions {
        add_vlan_tag_no_reg;
    }
}

register decCnt {
    width           : 8;
    static          : table_decrement_ttl;
    instance_count  : 3;
}

table table_decrement_ttl {
    // No reads statement, always run tagging
    actions {
        decrement_ttl;    
    }
}
