action add_vlan_tag(vid) {
    // Add the VLAN tag (overwrite if present)
    add_header(vlan_tag);

    // Setup the ethernet field value to vlan tag and copy the
    // ethertype value from ethernet to vlan tag. Copy the ether
    // type from the metadata field
    modify_field(vlan_tag.etherType,test_metadata.etherType);
    modify_field(ethernet.etherType,0x8100);

    // Setup the VLAN tag
    modify_field(vlan_tag.pcp,0);
    modify_field(vlan_tag.cfi,0);
    modify_field(vlan_tag.vid,vid);

    // Sent the VLAN to the output port 3
    modify_field(intrinsic_metadata.egress_port,3);

}

action add_vlan_tag_no_reg(vid) {
    // Add the VLAN tag (overwrite if present)
    add_header(vlan_tag);

    // Setup the ethernet field value to vlan tag and copy the
    // ethertype value from ethernet to vlan tag. Copy the ether
    // type from the metadata field
    modify_field(vlan_tag.etherType,test_metadata.etherType);
    modify_field(ethernet.etherType,0x8100);

    // Setup the VLAN tag
    modify_field(vlan_tag.pcp,0);
    modify_field(vlan_tag.cfi,0);
    modify_field(vlan_tag.vid,vid);

    // Sent the VLAN to the output port 3
    modify_field(intrinsic_metadata.egress_port,3);

}

action _permit()  {
    no_op();
}

action _drop() {
drop();
}

action decrement_ttl() {
    add_to_field(ipv4.ttl,-1);
    register_write(decCnt,0,ipv4.ttl);
}

table table_ipv4_filter {
    reads {
        ipv4.srcAddr : ternary;
    }

    actions {
        _permit;
        add_vlan_tag;
        _drop;
    }

    max_size: 511;
}

table table_vlan_tag {
    // No reads statement, always run tagging
    actions {
        add_vlan_tag_no_reg;
    }
}

register decCnt {
    width           : 8;
    static          : table_decrement_ttl;
    instance_count  : 3;
}

table table_decrement_ttl {
    // No reads statement, always run tagging
    actions {
        decrement_ttl;
    }
}
