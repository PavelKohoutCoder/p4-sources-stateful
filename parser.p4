// Prepare metadata ===========================================================
metadata test_metadata_t        test_metadata {egressPort : 3;}; 

// Initial starting point =====================================================
parser start {
    return parse_ethernet;
}

// Ethernet ===================================================================

#define ETHERTYPE_IPV4 0x0800
#define ETHERTYPE_IPV6 0x86dd
#define ETHERTYPE_VLAN 0x8100, 0x9100, 0x9200, 0x9300


header ethernet_t ethernet;
parser parse_ethernet {
    extract(ethernet);
    // Copy the value if ingress port to the egress port
    set_metadata(test_metadata.egressPort,intrinsic_metadata.ingress_port);
    // Setup egress port to 2
    set_metadata(intrinsic_metadata.egress_port,test_metadata.egressPort);
    // Setup the test metadata ethertype
    set_metadata(test_metadata.etherType,latest.etherType);
    return select(latest.etherType) {
        ETHERTYPE_VLAN : parse_vlan;
        ETHERTYPE_IPV4 : parse_ipv4;
        default : ingress;
    }
}

// VLAN =======================================================================


header vlan_tag_t vlan_tag;
parser parse_vlan {
    extract(vlan_tag);
    return select(latest.etherType) {
        ETHERTYPE_VLAN : parse_vlan;
        ETHERTYPE_IPV4 : parse_ipv4;
        default : ingress;
    }
}

// IPv4 =======================================================================

header ipv4_t ipv4;
parser parse_ipv4 {
    extract(ipv4);
    return ingress;
}
